#!/bin/bash
echo "------------------ User name on OS ------------------"
cat /etc/passwd | grep -v "nologin" | grep -v "/bin/false" | grep "/bin/bash" | cut -d : -f1 | grep -v 'root\|web\|admin\|networkadm'


echo "------------------ last -n 50 ------------------"
last -n 50

echo "------------------ netstat -natp ------------------"
netstat -natp | grep -v "127.0.0.1"

